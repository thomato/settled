import Vue from "vue";
import VueI18n from "vue-i18n";

Vue.use(VueI18n);

const numberFormats = {
    en: {
        currency: {
            style: "currency", 
            currency: "GBP",
            minimumFractionDigits: 0,
            maximumFractionDigits: 0
        }
    }
};

const messages = {
    en: {
        property: {
            flat: "{count}-bedroom flat",
            house: "{count}-bedroom house",
            bathrooms: "No bathrooms | 1 bathroom | {count} bathrooms",
            bedrooms: "No bedrooms | 1 bedroom | {count} bedrooms",
            receptionRooms: "No reception rooms | 1 reception room | {count} reception rooms",
            searchResult: "Sorry, no properties were found. | There is 1 result that matches your search. | There are {count} results that match your search."
        }
    }
};

const dateTimeFormats = {
    "en": {
        short: {
            year: "numeric", 
            month: "short", 
            day: "numeric"
        }
    }
};

export default new VueI18n({
    locale: process.env.VUE_APP_I18N_LOCALE || "en",
    fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || "en",
    messages,
    numberFormats,
    dateTimeFormats
})
