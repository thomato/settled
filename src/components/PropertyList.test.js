import { mount } from "@vue/test-utils";
import PropertyList from "./PropertyList";
import propertyData from "../assets/data/properties.json";

import i18n from "../i18n";

describe("PropertyList data", () => {

    const wrapper = mount(PropertyList, { i18n });

    let totalNumberOfProperties;
    let firstProperty;

    beforeAll(() => {
        totalNumberOfProperties = propertyData.length;
        firstProperty = propertyData[0];
    });

    it("should test against a property with numBedrooms equalling 3", () => {
        expect(firstProperty.numBedrooms).toEqual(3);
    });

    it("should return the same number of properties without a filter", () => {
        expect(wrapper.vm.filterProperties(propertyData, {propertyType: "", numBedrooms: "", maxPrice: ""})).toHaveLength(totalNumberOfProperties);
    });

    it("should pass a filter with an empty string value", () => {
        expect(wrapper.vm.filterPropertiesByNumBedrooms(firstProperty, '')).toEqual(true);
    });

    it("should match a property with 2 or more bedrooms", () => {
        expect(wrapper.vm.filterPropertiesByNumBedrooms(firstProperty, 2)).toEqual(true);
    });

    it("should match a property with 3 or more bedrooms", () => {
        expect(wrapper.vm.filterPropertiesByNumBedrooms(firstProperty, 3)).toEqual(true);
    });

});