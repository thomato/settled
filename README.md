## About

Written in Vue.js as a solution to the front-end test, written without any bootstrapping or Vue CLI.
Pluralisation and currency formatting is handled with Vue's i18n.

Install the NPM packages:

`npm install`

Run the test script (just a few unit test to help develop the filters):

`npm run test`

Start the local server:

`npm run serve`
